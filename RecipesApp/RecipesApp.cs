using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin;

namespace RecipesApp
{
    [Application]
    class RecipesApp : Application
    {
        public static RecipesApp Current { get; private set; }

        public RecipesApp(IntPtr handle, global::Android.Runtime.JniHandleOwnership transfer)
            : base(handle, transfer) {
                Current = this;
        }

        public override void OnCreate()
        {
            base.OnCreate();
            Insights.Initialize("e458433aaa3f2d1f8290e12d866202099ff96954", Current);
        }
    }
}