using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.Widget;
using Android.Util;
using Android.Views;
using Android.Widget;
using Javax.Security.Auth;
using RecipesApp.Controllers;
using RecipesApp.Models;
using RecipesApp.Views;
using RecipesDownloaderLibrary.Controllers.Recipe;
using RecipesDownloaderLibrary.Controllers.WrongApiData;
using RecipesDownloaderLibrary.Models.Recipe;
using Xamarin;

namespace RecipesApp.Adapters
{
    public class RecipesListAdapter : RecyclerView.Adapter
    {
        private List<BaseRecipeModel> _recipes = new List<BaseRecipeModel>();
        private TypedValue _typedValue = new TypedValue();
        private int _background;
        private Activity _context;
        private RecyclerView _recyclerViewParent;

        private int _total;
        private int _count;

        public event EventHandler LastItemShown;

        public RecipesListAdapter(Android.App.Activity context, RecyclerView parent)
        {
            _context = context;
            context.Theme.ResolveAttribute(Resource.Attribute.selectableItemBackground, _typedValue, true);
            _background = _typedValue.ResourceId;
            _recyclerViewParent = parent;
            _recyclerViewParent.ChildViewAttachedToWindow += ChildViewAttachedToWindowHandler;
        }


        public void AddRecipes(PagedBaseRecipes recipes)
        {
            _recipes.AddRange(recipes.BaseRecipes);
            _total = recipes.Total;
            _count += recipes.Count;
            NotifyDataSetChanged();
        }

        public void ClearData()
        {
            if (_recipes.Count > 0)
            {
                _total = 0;
                _count = 0;
                _recipes.Clear();
                NotifyDataSetChanged();
            }
        }

        private void ChildViewAttachedToWindowHandler(object sender, RecyclerView.ChildViewAttachedToWindowEventArgs args)
        {
            var currentItemPosition = _recyclerViewParent.GetChildLayoutPosition(args.View);
            if (_recipes.Count < _total && currentItemPosition == _count - 2)
            {
                if (LastItemShown != null)
                    LastItemShown(this, new EventArgs());
            }
        }


        public class ProgressBarViewHolder : RecyclerView.ViewHolder
        {
            public ProgressBar ProgressBar { get; set; }
            public ProgressBarViewHolder(View itemView)
                : base(itemView)
            {
                ProgressBar = itemView.FindViewById<ProgressBar>(Resource.Id.recipeListProgressBar);
            }
        }


        public class RecipeViewHolderEmptyText : RecyclerView.ViewHolder
        {
            public RecipeViewHolderEmptyText(View itemView)
                : base(itemView)
            {
            }
        }


        public class RecipeViewHolder : RecyclerView.ViewHolder
        {
            public ImageView ImageView { get; set; }
            public TextView NameTextView { get; set; }
            public TextView CuisineTextView { get; set; }

            public string Url { get; set; }

            public RecipeViewHolder(View itemView, Action<int> eventLisitener)
                : base(itemView)
            {
                ImageView = itemView.FindViewById<ImageView>(Resource.Id.recipeListItemNameImage);
                NameTextView = itemView.FindViewById<TextView>(Resource.Id.recipeListItemName);
                CuisineTextView = itemView.FindViewById<TextView>(Resource.Id.recipeListItemCuisine);
                itemView.Click += (sender, args) => eventLisitener(base.AdapterPosition);
            }
        }

        public override int GetItemViewType(int position)
        {
            if (_recipes.Count == 0) return (int)RecyclerViewType.EmptyView;
            if (position == _count) return (int)RecyclerViewType.ProgressBarView;
            return (int)RecyclerViewType.RecipeView;
        }


        public async override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            var holderType = holder.GetType();
            switch (holderType.Name)
            {
                case "RecipeViewHolder":
                    await ConfigureRecipeViewHolder(holder, position);
                    break;
                case "ProgressBarViewHolder":
                    ConfigureProgressBarViewHolder(holder);
                    break;
                case "RecipeViewHolderEmptyText":
                    break;
            }
        }

        private void ConfigureProgressBarViewHolder(RecyclerView.ViewHolder holder)
        {
            var progressBarViewHolder = (ProgressBarViewHolder)holder;
            progressBarViewHolder.ProgressBar.Indeterminate = true;
        }

        private async Task ConfigureRecipeViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            var recipeViewHolder = (RecipeViewHolder)holder;
            var recipe = _recipes[position];


            recipeViewHolder.Url = recipe.Url;

            recipeViewHolder.CuisineTextView.Text = WrongApiDataSymbolsController.DeleteBrokenSymbols(recipe.Cuisine);
            recipeViewHolder.NameTextView.Text = WrongApiDataSymbolsController.DeleteBrokenSymbols(recipe.Name);

            recipeViewHolder.ImageView.SetImageDrawable(new ColorDrawable(Color.WhiteSmoke));
            recipeViewHolder.ImageView.SetMinimumHeight(300);

            var cacheDirPath = _context.CacheDir.AbsolutePath;
            var cacheImageController = new CacheImageController();
            var filePath = cacheImageController.CreateCacheFilePathByUri(recipe.ImageUrl, cacheDirPath);
            var recipeImage = cacheImageController.GetCacheFile(filePath);

            if (recipeImage == null)
            {
                var imageDownloadController = new ImageDownloadController();
                recipeImage = await imageDownloadController.DownloadImage(recipe.ImageUrl);
                cacheImageController.SaveFile(filePath, recipeImage);
            }

            recipeViewHolder.ImageView.SetImageBitmap(recipeImage);
        }


        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            var inflater = LayoutInflater.From(parent.Context);
            View view;
            RecyclerView.ViewHolder viewHolder = null;

            switch (viewType)
            {
                case (int)RecyclerViewType.EmptyView:
                    view = inflater.Inflate(Resource.Layout.EmptyRecipeListLabel, parent, false);
                    view.SetBackgroundResource(_background);
                    viewHolder = new RecipeViewHolderEmptyText(view);
                    break;
                case (int)RecyclerViewType.RecipeView:
                    view = inflater.Inflate(Resource.Layout.RecipeListItem, parent, false);
                    view.SetBackgroundResource(_background);
                    viewHolder = new RecipeViewHolder(view, ItemClicked);
                    viewHolder.IsRecyclable = false;
                    break;
                case (int)RecyclerViewType.ProgressBarView:
                    view = inflater.Inflate(Resource.Layout.RecipeListProgressBarItem, parent, false);
                    view.SetBackgroundResource(_background);
                    viewHolder = new ProgressBarViewHolder(view);
                    break;
                default:
                    var exception = new System.Exception("Unexpected behavior. There aren't 3 types of view holders in a recipe list adapter in the app.");
                    Insights.Report(exception);
                    throw exception;
            }

            return viewHolder;
        }

        private void ItemClicked(int i)
        {
            var recipe = _recipes[i];
            var intent = new Intent(_context, typeof(DetailedRecipeActivity));
            intent.PutExtra(DetailedRecipeActivity.UrlTag, recipe.Url);

            _context.StartActivity(intent);
        }

        public override int ItemCount
        {
            get
            {
                if (_recipes.Count != 0 && _recipes.Count == _total) return _recipes.Count;
                return _recipes.Count + 1;
            }
        }
    }
}