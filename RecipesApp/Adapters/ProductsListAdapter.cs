using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Content.Res;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Support.V7.Widget;
using RecipesApp.Models;

namespace RecipesApp.Adapters
{
    public class ItemSelectedEventArgs : EventArgs
    {
        public List<string> SelectedItems { get; set; }
    }

    public class ProductsListAdapter :  RecyclerView.Adapter 
    {
        private TypedValue _typedValue = new TypedValue();
        private int _background;
        private List<string> _products = new List<string>();
        private List<string> _selectedProducts = new List<string>();
        private Activity _context;
        private RecyclerView _parentRecyclerView;

        public EventHandler<ItemSelectedEventArgs> ItemSelected;

        public ProductsListAdapter(Android.App.Activity context, RecyclerView parent)
        {
            _parentRecyclerView = parent;
            _context = context;
            context.Theme.ResolveAttribute(Resource.Attribute.selectableItemBackground, _typedValue, true);
            _background = _typedValue.ResourceId;
        }

        public class ViewHolderEmptyText : RecyclerView.ViewHolder
        {
            public ViewHolderEmptyText(View itemView) : base(itemView)
            {
            }
        }

        public class ProductViewHolder : RecyclerView.ViewHolder
        {
            public TextView TextView { get; set; }
            public LinearLayout LinearLayout { get; set; }

            public ProductViewHolder(View view, Action<LinearLayout,TextView> selectHandler)
                : base(view)
            {
                view.Click += (sender, args) => selectHandler(LinearLayout, TextView);
                TextView = view.FindViewById<TextView>(Resource.Id.productNameTxtV);
                LinearLayout = view.FindViewById<LinearLayout>(Resource.Id.productNameLayout);
            }

            public override string ToString()
            {
                return base.ToString() + " '" + TextView.Text;
            }
        }

        public override int GetItemViewType(int position)
        {
            if (_products.Count == 0) return (int)RecyclerViewType.EmptyView;
            return (int)RecyclerViewType.ProductView;
        }

        public String GetValueAt(int position)
        {
            return _products[position];
        }

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            if (holder is ProductViewHolder)
            {
                var bindedHolder = holder as ProductViewHolder;
                if (_products.Count > 0)
                {
                    var productName = _products[position];
                    bindedHolder.TextView.Text = productName;
                    if (!_selectedProducts.Contains(productName))
                    {
                        SetColorLinearLayout(bindedHolder.LinearLayout, Resource.Color.window_background); 
                    }
                    else
                    {
                        SetColorLinearLayout(bindedHolder.LinearLayout, Resource.Color.colorPrimary);
                    }

                } 
            }
        }

        private void SetArgsAndSendEvent(ItemSelectedEventArgs args)
        {
            args.SelectedItems = _selectedProducts;
            ItemSelected(this, args);
        }

        private void SetColorLinearLayout(LinearLayout linearLayout, int colorId)
        {
            var color = _context.Resources.GetColor(colorId);
            linearLayout.SetBackgroundColor(color);
        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            LayoutInflater inflater;
            View view;

            if (viewType == (int)RecyclerViewType.EmptyView)
            {
                inflater = LayoutInflater.From(parent.Context);
                view = inflater.Inflate(Resource.Layout.EmptyProductListLabel, parent, false);
                view.SetBackgroundResource(_background);
                return new ViewHolderEmptyText(view);   
            }

            inflater = LayoutInflater.From(parent.Context);
            view = inflater.Inflate(Resource.Layout.ProductListItem, parent, false);
            view.SetBackgroundResource(_background);
            return new ProductViewHolder(view, SelectionItemHandler);     
        }

        private void SelectionItemHandler(LinearLayout linearLayout, TextView textView)
        {
            if (linearLayout != null && textView != null)
            {
                var selectedText = textView.Text;
                var itemSelectedEventArgs = new ItemSelectedEventArgs();
                if (!_selectedProducts.Contains(selectedText))
                {
                    _selectedProducts.Add(selectedText);
                    SetColorLinearLayout(linearLayout, Resource.Color.colorPrimary);
                }
                else
                {
                    _selectedProducts.Remove(selectedText);
                    SetColorLinearLayout(linearLayout, Resource.Color.window_background);
                }

                SetArgsAndSendEvent(itemSelectedEventArgs);                
            }
        }
        

        public void AddProduct(string product)
        {
            if (_products.Count > 0)
            {
                _products.Insert(0, product);
                _parentRecyclerView.ScrollToPosition(0);
                NotifyItemInserted(0);
            }
            else
            {
                _products.Add(product);
                NotifyDataSetChanged();              
            }
        }

        public void RemoveProducts()
        {
            foreach (var selectedProduct in _selectedProducts)
            {
                RemoveProduct(selectedProduct);
            }
            _selectedProducts.Clear();
        }

        private void RemoveProduct(string product)
        {
            var removingIndex = _products.IndexOf(product);
            _products.Remove(product);
            NotifyItemRemoved(removingIndex);
        }

        public override int ItemCount
        {
            get { return _products.Count > 0 ? _products.Count : 1; }
        }
    }
}