using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Net;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.Widget;
using Android.Views;
using RecipesApp.Adapters;
using RecipesDownloaderLibrary.Controllers.Recipe;
using RecipesDownloaderLibrary.Models.Recipe;
using AlertDialog = Android.Support.V7.App.AlertDialog;

namespace RecipesApp.Views
{
    public class RecipeListFragment : Android.Support.V4.App.Fragment
    {
        private RecipesListAdapter _recipesListAdapter;
        private RecyclerView _recyclerView;
        private List<string> _products = null;

        private int _limit = 10;
        private int _offset = 0;

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var view = inflater.Inflate(Resource.Layout.FragmentRecipeList, container, false);
            _recyclerView = view.JavaCast<RecyclerView>();
            SetupRecyclerView(_recyclerView);
            return view;
        }

        private void SetupRecyclerView(RecyclerView rv)
        {
            _recipesListAdapter = new RecipesListAdapter(Activity, rv);
            _recipesListAdapter.LastItemShown += UpdateData;

            rv.SetLayoutManager(new LinearLayoutManager(rv.Context));
            rv.SetAdapter(_recipesListAdapter);
        }

        private async void UpdateData(object sender, EventArgs eventArgs)
        {
            _offset += _limit;
            var pagedBaseRecipes = await GetRecipes(_products, _offset, _limit);
            ShowRecipes(pagedBaseRecipes, _recipesListAdapter);
        }

        private void ClearPagingAndRecipes()
        {
            _recipesListAdapter.ClearData();
        }

        public async Task ShowRecipesByProducts(IList<string> products)
        {
            var connectivityManager = (ConnectivityManager)Activity.GetSystemService(Context.ConnectivityService);
            var activeConnection = connectivityManager.ActiveNetworkInfo;
            if ((activeConnection != null) && activeConnection.IsConnected)
            {
                var progressDialog = ProgressDialog.Show(Activity, 
                    GetString(Resource.String.DownloadProgressBarTitle), 
                    GetString(Resource.String.DownloadProgressBarMessage));

                _products = new List<string>(products);
                ClearPagingAndRecipes();

                var pagedBaseRecipes = await GetRecipes(products, _offset, _limit);
                ShowRecipes(pagedBaseRecipes, _recipesListAdapter);

                progressDialog.Hide();
            }
            else
            {
                var builder = new AlertDialog.Builder(Activity);
                builder.SetTitle(GetString(Resource.String.NoInternetConnectionTitle));
                builder.SetMessage(GetString(Resource.String.NoInternetConnectionMessage));
                builder.Show();
            }

        }

        private async Task<PagedBaseRecipes>  GetRecipes(IList<string> products, int offset, int limit)
        {
            var url = Activity.Resources.GetString(Resource.String.ApiUrl);
            var recipeController = new RecipeController();
            var downloadedPagedRecipes = await recipeController.GetPagedRecipesByProducts(products, url, limit, offset);
            return downloadedPagedRecipes;
        }

        private void ShowRecipes(PagedBaseRecipes pagedBaseRecipes, RecipesListAdapter recipesListAdapter)
        {
            if (pagedBaseRecipes != null && pagedBaseRecipes.BaseRecipes != null)
            {
                recipesListAdapter.AddRecipes(pagedBaseRecipes);
            }
        }
    }
}