using System;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.Widget;
using Android.Views;
using RecipesApp.Adapters;

namespace RecipesApp.Views
{
    public class ManageProductsFragment : Android.Support.V4.App.Fragment
    {
        private ProductsListAdapter _productsListAdapter;

        public EventHandler<ItemSelectedEventArgs> ItemSelected;


        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var view = inflater.Inflate(Resource.Layout.FragmentManageProducts, container, false);
            var rv = view.JavaCast<RecyclerView>();
            SetupRecyclerView(rv, savedInstanceState);
            return view;
        }


        private void SetupRecyclerView(RecyclerView rv, Bundle savedInstanceState)
        {
            rv.SetLayoutManager(new LinearLayoutManager(rv.Context));
            rv.HasFixedSize = true;
            _productsListAdapter = new ProductsListAdapter(Activity, rv);

            _productsListAdapter.ItemSelected += (sender, args) => ItemSelected(new object(), args);

            rv.SetAdapter(_productsListAdapter);
        }


        public void AddProduct(String product)
        {
            _productsListAdapter.AddProduct(product);
        }

        public void RemoveProducts()
        {
            _productsListAdapter.RemoveProducts();
        }
    }
}