﻿using System;
using System.Collections.Generic;
using System.Linq;
using Android.App;
using Android.OS;
using Android.Support.Design.Widget;
using Android.Support.V4.View;
using Android.Support.V7.App;
using Android.Support.V7.Widget;
using Android.Text;
using Android.Views;
using Android.Widget;
using RecipesApp.Adapters;
using Xamarin;
using AlertDialog = Android.Support.V7.App.AlertDialog;
using String = System.String;

namespace RecipesApp.Views
{
    [Activity(Label = "Recipe list", MainLauncher = true, Icon = "@drawable/icon", ConfigurationChanges = Android.Content.PM.ConfigChanges.Orientation | Android.Content.PM.ConfigChanges.ScreenSize)]
    public class MainActivity : AppCompatActivity
    {
        private IList<string> _products = new List<string>();
        private IList<string> _selectedProducts;

        private CustomFragmentAdapter _customFragmentAdapter;
        private IMenuItem _removeMenuItem;

        private FloatingActionButton _addProductFab;
        private FloatingActionButton _searchFab;

        private ViewPager _viewPager;

        private ManageProductsFragment ManageProductFragment
        {
            get
            {
                var manageProductFragment = _customFragmentAdapter.GetItem(0) as ManageProductsFragment;
                return manageProductFragment;
            }
        }

        private RecipeListFragment RecipeListFragment
        {
            get
            {
                var recipeListFragment = _customFragmentAdapter.GetItem(1) as RecipeListFragment;
                return recipeListFragment;
            }
        }

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.Main);

            var toolbar = FindViewById<Toolbar>(Resource.Id.addProductsToolbar);
            SetSupportActionBar(toolbar);

            _viewPager = FindViewById<Android.Support.V4.View.ViewPager>(Resource.Id.addProductsViewpager);
            if (_viewPager != null)
            {
                SetupViewPager(_viewPager);
                _viewPager.PageSelected += ViewPagerOnPageSelected;
            }

            var tabLayout = FindViewById<TabLayout>(Resource.Id.addProductsTabs);
            tabLayout.SetupWithViewPager(_viewPager);

            _addProductFab = FindViewById<FloatingActionButton>(Resource.Id.addProductFab);
            _addProductFab.Click += AddProductFabOnClick;

            _searchFab = FindViewById<FloatingActionButton>(Resource.Id.searchFab);
            _searchFab.Click += SearchFabOnClick;
        }


        private async void SearchFabOnClick(object sender, EventArgs eventArgs)
        {
            if (_products.Count > 0)
            {
                _viewPager.CurrentItem = 1;
                await RecipeListFragment.ShowRecipesByProducts(_products);
            }
            else
            {
                ShowSnackBar(_addProductFab, GetString(Resource.String.NoChosenProducts), Snackbar.LengthLong);
            }
        }

        private void ViewPagerOnPageSelected(object sender, ViewPager.PageSelectedEventArgs pageSelectedEventArgs)
        {
            var currentPage = _viewPager.CurrentItem;
            switch (currentPage)
            {
                case 0:
                    _addProductFab.Show();
                    _searchFab.Show();
                    break;
                case 1:
                    _addProductFab.Hide();
                    _searchFab.Hide();
                    break;
                default:
                    var exception = new System.Exception("Unexpected behavior. There aren't 3 tabs in the app.");
                    Insights.Report(exception);
                    throw exception;
            }
        }


        private void AddProductFabOnClick(object sender, EventArgs eventArgs)
        {
            var builder =
                  new AlertDialog.Builder(this);
            builder.SetTitle(GetString(Resource.String.AddProductDialogTitle));

            var inputField = new EditText(this);
            var inputFilter = new InputFilterLengthFilter(25);
            inputField.SetFilters(new IInputFilter[] { inputFilter });

            builder.SetView(inputField);

            builder.SetNegativeButton(GetString(Resource.String.CancelLabel), (o, args) => { });
            builder.SetPositiveButton(GetString(Resource.String.AddLabel), (o, args) =>
            {
                var inputData = inputField.Text;
                inputData = inputData.Trim().ToLower();
                if (!string.IsNullOrEmpty(inputData))
                {
                    if (!_products.Contains(inputData))
                    {
                        _products.Add(inputData);
                        ManageProductFragment.AddProduct(inputData);
                        ShowSnackBar(_addProductFab, String.Format(GetString(Resource.String.AddedProductLabelTemplate), inputData), Snackbar.LengthShort);
                    }
                    else ShowSnackBar(_addProductFab, String.Format(GetString(Resource.String.ExistingProductLabel), inputData), Snackbar.LengthShort);
                }
                else
                {
                    ShowSnackBar(_addProductFab, GetString(Resource.String.EmptyInputLabel), Snackbar.LengthShort);
                }
            });

            builder.Show();
        }

        private void SetupViewPager(Android.Support.V4.View.ViewPager viewPager)
        {
            _customFragmentAdapter = new CustomFragmentAdapter(SupportFragmentManager);

            var manageProductsFragment = new ManageProductsFragment();
            _customFragmentAdapter.AddFragment(manageProductsFragment, GetString(Resource.String.ProductFragmentTitle));

            var recipeListFragment = new RecipeListFragment();
            _customFragmentAdapter.AddFragment(new RecipeListFragment(), GetString(Resource.String.RecipeFragmentTitle));

            viewPager.Adapter = _customFragmentAdapter;

            manageProductsFragment.ItemSelected += (sender, args) =>
            {
                var selectedItemsCount = args.SelectedItems.Count;
                _selectedProducts = new List<string>(args.SelectedItems);
                CalculateVisibilityRemoveButton(selectedItemsCount);
            };
        }

        private void CalculateVisibilityRemoveButton(int selectedItemsCount)
        {
            if (selectedItemsCount > 0 && !_removeMenuItem.IsVisible) _removeMenuItem.SetVisible(true);
            if (selectedItemsCount == 0 && _removeMenuItem.IsVisible) _removeMenuItem.SetVisible(false);
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.ManageProductsMenu, menu);

            _removeMenuItem = menu.FindItem(Resource.Id.addProductRemoveMenuItem);
            _removeMenuItem.SetVisible(false);

            return true;
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Resource.Id.addProductRemoveMenuItem:
                    ManageProductFragment.RemoveProducts();
                    _viewPager.CurrentItem = 0;
                    _products = _products.Except(_selectedProducts).ToList();
                    CalculateVisibilityRemoveButton(0);
                    break;
            }
            return base.OnOptionsItemSelected(item);
        }

        private void ShowSnackBar(View parentView, String text, int duration)
        {
            var snackBar = Snackbar.Make(parentView, text, duration);
            snackBar.Show();
        }
    }
}

