using System;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Net;
using Android.OS;
using Android.Support.Design.Widget;
using Android.Support.V7.App;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using RecipesApp.Controllers;
using RecipesDownloaderLibrary.Controllers.Recipe;
using RecipesDownloaderLibrary.Controllers.WrongApiData;
using RecipesDownloaderLibrary.Models.Recipe;
using AlertDialog = Android.Support.V7.App.AlertDialog;

namespace RecipesApp.Views
{
    [Activity(Label = "Info", ConfigurationChanges = Android.Content.PM.ConfigChanges.Orientation | Android.Content.PM.ConfigChanges.ScreenSize)]  
    public class DetailedRecipeActivity : AppCompatActivity
    {
        public const string UrlTag = "recipe_url";

        private string _showingIngridientTemplate = "{0} - {1}\n";
        private string _showingDirectionsTemplate = "{0} - {1}\n";
        private DetailedRecipeModel _recipe;
        private RecipeController _recipeController;


        protected override async void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.DetailedRecipe);

            var connectivityManager = (ConnectivityManager)this.GetSystemService(Context.ConnectivityService);
            var activeConnection = connectivityManager.ActiveNetworkInfo;
            if ((activeConnection != null) && activeConnection.IsConnected)
            {
                SetUpToolBar();

                var progressDialog = ProgressDialog.Show(this, GetString(Resource.String.DownloadProgressBarTitle), GetString(Resource.String.DownloadProgressBarMessage));

                _recipe = await GetRecipe();

                SetCollapsingToolbar(_recipe);
                SetIngridientsView(_recipe);
                SetDirectionsView(_recipe);

                progressDialog.Hide();
                await LoadImage(_recipe);
            }
            else
            {
                ShowInternetConnetionProblemsMessage();
            }
        }


        private void ShowInternetConnetionProblemsMessage()
        {
            var builder = new AlertDialog.Builder(this);
            builder.SetTitle(GetString(Resource.String.NoInternetConnectionTitle));
            builder.SetMessage(GetString(Resource.String.NoInternetConnectionMessage));
            builder.Show();
        }

        private void SetCollapsingToolbar(DetailedRecipeModel recipe)
        {
            if (recipe != null)
            {
                var collapsingToolbar = FindViewById<CollapsingToolbarLayout>(Resource.Id.detailedRecipeCollapsingBar);
                collapsingToolbar.SetTitle(_recipe.Name);
            }
        }

        private void SetIngridientsView(DetailedRecipeModel recipe)
        {
            if (recipe != null && recipe.Ingredients != null)
            {
                var ingridientsLinearLayout = FindViewById<LinearLayout>(Resource.Id.detailedRecipeIngridientsLayout);
                var ingridientsTextView = CreateTextViewForCardView();
                foreach (var ingridientModel in recipe.Ingredients)
                {
                    var fixedName = WrongApiDataSymbolsController.DeleteBrokenSymbols(ingridientModel.Name);
                    ingridientsTextView.Text += String.Format(_showingIngridientTemplate, fixedName,
                        ingridientModel.Quantity);
                }
                ingridientsLinearLayout.AddView(ingridientsTextView);
            }
        }

        private void SetDirectionsView(DetailedRecipeModel recipe)
        {
            if (recipe != null && recipe.Directions != null)
            {
                var directionsLinearLayout = FindViewById<LinearLayout>(Resource.Id.detailedRecipeDirectionsLayout);
                var directionsTextView = CreateTextViewForCardView();
                int position = 1;
                foreach (var direction in recipe.Directions)
                {
                    var fixedDirection = WrongApiDataSymbolsController.DeleteBrokenSymbols(direction);
                    directionsTextView.Text += String.Format(_showingDirectionsTemplate, position, fixedDirection);
                    position++;
                }
                directionsLinearLayout.AddView(directionsTextView);
            }
        }

        private TextView CreateTextViewForCardView()
        {
            var textView = new TextView(this)
            {
                LayoutParameters =
                    new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WrapContent, ViewGroup.LayoutParams.WrapContent)
            };
            return textView;
        }


        private async Task<DetailedRecipeModel> GetRecipe()
        {
            var recipeUrl = Intent.GetStringExtra(UrlTag);
            _recipeController = new RecipeController();
            return await _recipeController.GetDetailedRecipe(recipeUrl);
        }

        private async Task LoadImage(DetailedRecipeModel recipe)
        {
            if (recipe != null)
            {
                var imageCollapsingToolbar = FindViewById<ImageView>(Resource.Id.detailedRecipeAppBarImage);
                var cacheImageController = new CacheImageController();
                var loadedImage = cacheImageController.GetCacheFile(recipe.ImageUrl, this.CacheDir.AbsolutePath);

                if (loadedImage == null)
                {
                    var imageDownloadController = new ImageDownloadController();
                    loadedImage = await imageDownloadController.DownloadImage(recipe.ImageUrl);

                }

                imageCollapsingToolbar.SetImageBitmap(loadedImage);
            }
        }

        private void SetUpToolBar()
        {
            var toolbar = FindViewById<Toolbar>(Resource.Id.detailedRecipeToolbar);
            SetSupportActionBar(toolbar);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
        }


        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    this.Finish();
                    return true;
            }
            return base.OnOptionsItemSelected(item);
        }
    }
}