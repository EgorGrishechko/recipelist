using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace RecipesApp.Models
{
    public enum RecyclerViewType
    {
        EmptyView = 165,
        ProductView = 166,
        RecipeView = 167,
        ProgressBarView = 168
    }
}
