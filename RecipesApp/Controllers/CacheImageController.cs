using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin;

namespace RecipesApp.Controllers
{
    public class CacheImageController
    {
        private string _fileExtension = ".png";
        private static object _fileWriteLock = new object();

        public Bitmap GetCacheFile(string uri, string cacheDirectoryPath)
        {
            var pathToFile = CreateCacheFilePathByUri(uri, cacheDirectoryPath);
            return GetCacheFile(pathToFile);
        }

        public Bitmap GetCacheFile(string pathToFile)
        {
            try
            {
                if (File.Exists(pathToFile))
                {
                    var imageData = File.ReadAllBytes(pathToFile);
                    var decodeImage = BitmapFactory.DecodeByteArray(imageData, 0, imageData.Length);
                    return decodeImage;
                }
            }
            catch (Exception ex)
            {
                Insights.Report(ex);
                return null;
            }
            return null;
        }

        public string CreateCacheFilePathByUri(string uri, string cacheDirectoryPath)
        {
            var fileName = CreateFileNameByUri(uri);
            var cacheFilePath = System.IO.Path.Combine(cacheDirectoryPath, fileName); ;
            return cacheFilePath;
        }

        public string CreateFileNameByUri(string uri)
        {
            var hash = Math.Abs(uri.GetHashCode());
            var fileName = hash + _fileExtension;
            return fileName;
        }

        public void SaveFile(string path, Bitmap image)
        {
            lock (_fileWriteLock)
            {
                if (image != null && !File.Exists(path))
                {
                    using (var stream = new FileStream(path, FileMode.Create))
                    {
                        image.Compress(Bitmap.CompressFormat.Png, 100, stream);                        
                    }
                }
            }
        }
    }
}