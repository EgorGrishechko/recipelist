using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using RecipesDownloaderLibrary.Controllers.Recipe;
using Xamarin;

namespace RecipesApp.Controllers
{
    public class ImageDownloadController
    {
        public async Task<Bitmap> DownloadImage(string uri)
        {
            Uri finalUri;
            if (!Uri.TryCreate(uri, UriKind.Absolute, out finalUri))
            {
                return null;
            }

            return await DownloadImage(finalUri);
        }
        public async Task<Bitmap> DownloadImage(Uri uri)
        {
            try
            {
                var webController = new WebController();
                var imageData = await webController.GetDataFromUrl(uri);
                if (imageData != null && imageData.Length > 0)
                {
                    var decodeImage = BitmapFactory.DecodeByteArray(imageData, 0, imageData.Length);
                    return decodeImage;                    
                }

                return null;

            }
            catch (Exception ex)
            {
                Insights.Report(ex);
                return null;
            }

        }
    }
}