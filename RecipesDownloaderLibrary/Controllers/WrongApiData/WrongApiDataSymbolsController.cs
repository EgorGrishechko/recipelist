﻿using System;
using System.Text;

namespace RecipesDownloaderLibrary.Controllers.WrongApiData
{
    public class WrongApiDataSymbolsController
    {
        public static string DeleteBrokenSymbols(string inputData)
        {
            var brokenSymbols = new string[] { "¶", "â", "â", "¹", "º", "»", "¼", "Â", "¸", "·", "¹", "º", "»", "¼", "Ã", "©", "ª", "€”", "„" };
            var stringBuilder = new StringBuilder(inputData);
            foreach (var brokenSymbol in brokenSymbols)
            {
                stringBuilder.Replace(brokenSymbol, String.Empty);
            }

            var fixedString = stringBuilder.ToString();
            fixedString = fixedString.Trim();

            return fixedString;
        }
    }
}
