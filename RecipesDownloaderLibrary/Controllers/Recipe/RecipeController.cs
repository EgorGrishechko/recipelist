﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RecipesDownloaderLibrary.Models.Recipe;
using Xamarin;

namespace RecipesDownloaderLibrary.Controllers.Recipe
{
    public class RecipeController
    {
        private WebController _webController;
        private string _ampersandCode = "&amp;";

        public async Task<PagedBaseRecipes> GetPagedRecipesByProducts(IList<string> products, string apiUrl, int limit, int offset)
        {
            string requestRightPart = string.Empty;
            List<BaseRecipeModel> recipes = null;
            PagedBaseRecipes pagedBaseRecipes = null;

            var tempProductList = new List<string>(products);
            if (tempProductList.Count != 0)
            {
                requestRightPart += tempProductList[0];
                tempProductList.RemoveAt(0);

                foreach (var product in tempProductList)
                {
                    requestRightPart += _ampersandCode;
                    requestRightPart += product;
                }

                var requestUrl = String.Format(apiUrl, requestRightPart, limit, offset);
                Uri finalUri;
                if (!Uri.TryCreate(requestUrl, UriKind.Absolute, out finalUri))
                {
                    return null;
                }

                try
                {
                    _webController = new WebController();
                    var results = await _webController.GetStringFromUrl(finalUri);

                    if (results != null)
                    {
                        var jObject = JObject.Parse(results);
                        var tempJsonData = jObject["results"].ToString();
                        recipes = JsonConvert.DeserializeObject<List<BaseRecipeModel>>(tempJsonData);

                        pagedBaseRecipes = new PagedBaseRecipes
                        {
                            BaseRecipes = recipes,
                            Count = jObject["count"].ToObject<Int32>(),
                            Limit = jObject["limit"].ToObject<Int32>(),
                            Offset = jObject["offset"].ToObject<Int32>(),
                            Total = jObject["total"].ToObject<Int32>()
                        };
                    }

                }
                catch (Exception ex)
                {
                    Insights.Report(ex);
                }
            }
            return pagedBaseRecipes;
        }

        public async Task<DetailedRecipeModel> GetDetailedRecipe(string url)
        {
            if (!string.IsNullOrEmpty(url))
            {
                try
                {
                    Uri finalUri;
                    if (!Uri.TryCreate(url, UriKind.Absolute, out finalUri))
                    {
                        return null;
                    }
                    _webController = new WebController();
                    var result = await _webController.GetStringFromUrl(finalUri);

                    if (result == null) return null;

                    var recipe = JsonConvert.DeserializeObject<DetailedRecipeModel>(result);
                    return recipe;                    
                }
                catch (Exception ex)
                {
                    Insights.Report(ex);
                }
            }
            return null;
        }
    }
}
