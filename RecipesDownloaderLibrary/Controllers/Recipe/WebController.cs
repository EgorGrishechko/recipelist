﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using Xamarin;

namespace RecipesDownloaderLibrary.Controllers.Recipe
{
    public class WebController
    {
        private HttpClient _httpClient;

        public async Task<string> GetStringFromUrl(Uri uri)
        {
            string result = string.Empty;

            try
            {
                using (_httpClient = new HttpClient())
                {
                    _httpClient.Timeout = new TimeSpan(0, 0, 20);
                    result = await _httpClient.GetStringAsync(uri);
                }
            }
            catch (Exception ex)
            {
                Insights.Report(ex);
            }

            return result;
        }


        public async Task<byte[]> GetDataFromUrl(Uri uri)
        {
            byte[] result = null;

            try
            {
                using (_httpClient = new HttpClient())
                {
                    _httpClient.Timeout = new TimeSpan(0, 1, 0);
                    result = await _httpClient.GetByteArrayAsync(uri);
                }
            }
            catch (Exception ex)
            {
                Insights.Report(ex);
            }

            return result;
        }
    }
}
