﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace RecipesDownloaderLibrary.Models.Recipe
{
    public class DetailedRecipeModel : BaseRecipeModel
    {
        public string Cost { get; set; }
        public List<string> Directions { get; set; }
        
        [JsonProperty(PropertyName = "nutritional_info")]
        public Dictionary<String, String> NutritionalInfo { get; set; }

        public new List<IngridientModel> Ingredients { get; set; }
    }
}
