﻿namespace RecipesDownloaderLibrary.Models.Recipe
{
    public class IngridientModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Quantity { get; set; }
        public string Url { get; set; }
    }
}
