﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecipesDownloaderLibrary.Models.Recipe
{
    public class PagedBaseRecipes
    {
        public List<BaseRecipeModel> BaseRecipes { get; set; }
        public int Count { get; set; }
        public int Offset { get; set; }
        public int Limit { get; set; }
        public int Total { get; set; }
    }
}
