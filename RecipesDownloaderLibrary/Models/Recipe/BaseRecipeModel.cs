﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace RecipesDownloaderLibrary.Models.Recipe
{
    public class BaseRecipeModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public string Cuisine { get; set; }

        [JsonProperty(PropertyName = "cooking_method")]
        public string CookingMethod { get; set; }
        public virtual List<string> Ingredients { get; set; }

        [JsonProperty(PropertyName = "image")]
        public string ImageUrl { get; set; }
        [JsonProperty(PropertyName = "thumb")]
        public string ThumbUrl { get; set; }
    }
}
